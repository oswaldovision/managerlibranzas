exports.remove = function (model, key, value, res) {
    var filterArg = '{"' + key + '":' + '"' + value + '"}';
    var filter = JSON.parse(filterArg);
    model.findOne(filter, function (error, data) {
        if (error) {
            console.log(error);
            if (res != null) {
                res.writeHead(500, { 'Content-Type': 'text/plain' });
                res.end('Internal server error');
            }
            return;
        } else {
            if (!data) {
                if (res != null) {
                    res.writeHead(404, { 'Content-Type': 'text/plain' });
                    res.end('Not Found');
                }
                return;
            } else {
                data.remove(function (error) {
                    if (!error) {
                        data.remove();
                    }
                    else {
                        console.log(error);
                    }
                });

                if (res != null) {
                    res.send('Deleted');
                }
                return;
            }
        }
    });
};

exports.update = function (model, requestBody, response) {
    var _username = requestBody.username;
    model.findOne({ username: _username }, function (error, data) {
        if (error) {
            console.log(error);
            if (response != null) {
                response.writeHead(500, { 'Content-Type': 'text/plain' });
                response.end('Internal server error');
            }
            return;
        } else {
            var user = toUser(requestBody, model);
            if (!data) {
                console.log('User: ' + _username + ' does not exist. It will be created.');
                user.save(function (error) {
                    if (!error)
                        user.save();
                });

                if (response != null) {
                    response.writeHead(201, {
                        'Content-Type':
                        'text/plain'
                    });
                    response.end('Created');
                }
                return;
            }

            data.username = user.username;
            data.password = user.password;
            data.role = user.role;

            data.save(function (error) {
                if (!error) {
                    console.log('Successfully updated user: ' + _username);
                    data.save();
                } else {
                    console.log('Error on save operation');
                }
            });
            if (response != null) {
                response.send('Updated');
            }
        }
    });
};

exports.create = function (model, res) {
    var result = model.validateSync();
    if (result) {
        if (res != null) {
            res.writeHead(500, { 'Content-Type': 'text/plain' });
            res.end('Error en validacion de esquema: ' + result);
        }
        return;
    }
    model.save(function (error) {
        if (!error) {
            model.save();
            res.send('Created !');
        } else {
            if (res != null) {
                res.writeHead(500, { 'Content-Type': 'text/plain' });
                res.end('Error saving: ' + error);
            }
            return;
        }
    });
};

exports.query_by_arg = function (model, key, value, response) {
    //build a JSON string with the attribute and the value
    var filterArg = '{"' + key + '":' + '"' + value + '"}';
    var filter = JSON.parse(filterArg);
    model.find(filter, function (error, result) {
        if (error) {
            console.error(error);
            response.writeHead(500, {
                'Content-Type':
                'text/plain'
            });
            response.end('Internal server error');
            return;
        } else {
            if (!result) {
                if (response != null) {
                    response.writeHead(404, { 'Content-Type': 'text/plain' });
                    response.end('Not Found');
                }
                return;
            }
            if (response != null) {
                response.setHeader('Content-Type', 'application/json');
                response.send(result);
            }
        }
    });
};

exports.list = function (model, response) {
    model.find({}, function (error, result) {
        if (error) {
            console.error(error);
            return null;
        }
        if (response != null) {
            response.setHeader('content-type',
                'application/json');
            response.end(JSON.stringify(result));
        }
        return JSON.stringify(result);
    });
}