var express = require("express");
var path = require("path");
var xml = require('xml');
var cors = require('cors');
var loger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var mongoose = require('mongoose');
var http = require("http");

//Modules to Authentication
var passport = require('passport'),
    BasicStrategy = require('passport-http').BasicStrategy,
    apicache = require('apicache');

//routes
var userRoute = require('./routes/user');
var employedRoute = require('./routes/employed');
var companyRoute = require('./routes/company');
var financialRoute = require('./routes/financial');
var requestRoute = require('./routes/request');

//schema
var User = require('./schemas/user');

var app = express();
var cache = apicache.middleware;
app.use(passport.initialize());
app.use(bodyParser.json());
app.use(cors());

//midleware auth
passport.use(new BasicStrategy(function (username, password, done) {
    User.findOne({ username: username, password: password }, function (error, user) {
        if (error) {
            console.log(error);
            return done(error);
        } else {
            if (!user) {
                console.log('unknown user');
                return done(error);
            } else {
                console.log(user.username + ' authenticated successfully !');
                return done(null, user);
            }
        }
    });
}));

var db = mongoose.connection;
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/libranzasDb');

app.use('/v1/user/', passport.authenticate('basic', { session: false }), userRoute);

app.use('/v1/employed',passport.authenticate('basic', { session: false }), employedRoute);

app.use('/v1/company',passport.authenticate('basic', { session: false }), companyRoute);

app.use('/v1/financial',passport.authenticate('basic', { session: false }), financialRoute);

app.use('/v1/request',passport.authenticate('basic', { session: false }), requestRoute);

http.createServer(app).listen(3000, function () {
    console.log('Express server listening on port: 3000');
});