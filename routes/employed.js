var express = require('express');
var url = require("url");
var router = express.Router();

var repository = require('../modules/repository');
var schema = require('../schemas/employed');

//constant
var keySchema = 'numDoc';
var roleAdmin = 'admin';

router.get('/:numDoc', function (req, res) {
    repository.query_by_arg(schema, keySchema, req.params.numDoc, res);
});

router.get('/', function (req, res) {
    var get_params = url.parse(req.url, true).query;
    if (Object.keys(get_params).length == 0) {
        repository.list(schema, res);
    } else {
        var key = Object.keys(get_params)[0];
        var value = get_params[key];
        JSON.stringify(repository.query_by_arg(schema, key, value, res));
    }
});

router.put('/', function (req, res) {
    var employed = toEmployed(req.body);
    repository.create(employed, res);
});

router.delete('/:numDoc', function (req, res) {
    if (authorize(req.user, res)) {
        repository.remove(schema, keySchema, req.params.numDoc, res);
    } else {
        res.writeHead(403, { 'Content-Type': 'text/plain' });
        res.end('Forbidden')
        return;
    }
});

function toEmployed(body) {
    return new schema(
        {
            numDoc: body.numDoc,
            firstName: body.firstName,
            lastName: body.lastName,
            typeDocument: body.typeDocument,
            position: body.position
        });
}

function authorize(user) {
    if ((user == null) || (user.role != roleAdmin)) {
        return false;
    }
    return true;
}

module.exports = router;
