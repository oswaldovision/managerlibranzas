var express = require('express');
var url = require("url");
var router = express.Router();

var repository = require('../modules/repository');
var model = require('../schemas/user');

//constant
var keySchema = 'username';
var roleAdmin = 'admin';

router.get('/:username', function (req, res) {
    repository.query_by_arg(model, keySchema, req.params.username, res);
});

router.get('/', function (req, res) {
    var get_params = url.parse(req.url, true).query;
    if (Object.keys(get_params).length == 0) {
        repository.list(model, res);
    } else {
        var key = Object.keys(get_params)[0];
        var value = get_params[key];
        JSON.stringify(repository.query_by_arg(model, key, value, res));
    }
});

router.put('/', function (req, res) {
    var user = toUser(req.body);
    repository.create(user, res);
});

router.delete('/:username', function (req, res) {
    if (authorize(req.user, res)) {
        repository.remove(model, keySchema, req.params.username, res);
    } else {
        res.writeHead(403, { 'Content-Type': 'text/plain' });
        res.end('Forbidden')
        return;
    }
});

function toUser(body) {
    return new model(
        {
            username: body.username,
            password: body.password,
            role: body.role
        });
}

function authorize(user) {
    if ((user == null) || (user.role != roleAdmin)) {
        return false;
    }
    return true;
}

module.exports = router;