var express = require('express');
var url = require("url");
var router = express.Router();

var repository = require('../modules/repository');
var schema = require('../schemas/request');

//constant
var keySchema = '_id';
var roleAdmin = 'admin';

router.get('/:id', function (req, res) {
    repository.query_by_arg(schema, keySchema, req.params.id, res);
});

router.get('/', function (req, res) {
    var get_params = url.parse(req.url, true).query;
    if (Object.keys(get_params).length == 0) {
        repository.list(schema, res);
    } else {
        var key = Object.keys(get_params)[0];
        var value = get_params[key];
        JSON.stringify(repository.query_by_arg(schema, key, value, res));
    }
});

router.put('/', function (req, res) {
    var financial = toRequest(req.body);
    repository.create(financial, res);
});

router.delete('/:id', function (req, res) {
    if (authorize(req.user, res)) {
        repository.remove(schema, keySchema, req.params.id, res);
    } else {
        res.writeHead(403, { 'Content-Type': 'text/plain' });
        res.end('Forbidden')
        return;
    }
});

function toRequest(body) {
    return new schema(
        {
            dateRequest: body.dateRequest,
            ownerId: body.ownerId,
            financialId: body.financialId,
            employedId: body.employedId,
            ammount: body.ammount,
            price: body.price,
            salesvalue: body.salesvalue
        });
}

function authorize(user) {
    if ((user == null) || (user.role != roleAdmin)) {
        return false;
    }
    return true;
}

module.exports = router;
