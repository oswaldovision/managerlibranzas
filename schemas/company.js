var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var employed = new Schema({
    nit: { type: String, index: { unique: true } },
    name: { type: String, required: true },
    phones: [String],
    address: { type: String }
});

module.exports = mongoose.model('company', employed);