var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var possibleRoles = ['admin', 'employed', 'financial','company'];

var authSchema = new Schema({
    username: { type: String, index: { unique: true } },
    password: { type: String, required : true },
    role: {type : String, enum :possibleRoles , required : true },
});

module.exports = mongoose.model('User', authSchema);