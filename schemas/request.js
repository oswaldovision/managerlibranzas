var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var request = new Schema({
    dateRequest: { type: Date, default: Date.now },
    ownerId: { type: String },
    financialId: {type : String, required : true },
    employedId: {type : String, required : true },
    ammount: {type : Number, required : true },
    price: {type : Number },
    salesvalue: {type : Number },
});

module.exports = mongoose.model('request', request);